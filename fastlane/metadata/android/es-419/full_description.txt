Mantén tu comunidad segura con Círculo, una aplicación fácil de usar que proporciona una forma rápida y segura de enviar alertas.

Círculo es una forma segura de compartir tu ubicación y estado con un grupo de amigos cercanos, familiares o colegas. Te ayuda a crear una red sólida de apoyo entre 6 personas

Todo lo que se comparte dentro de la aplicación está encriptado y se mantiene seguro en tu teléfono.